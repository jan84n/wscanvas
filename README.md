# WS Canvas

### Usage

```
# Install all the dev tools
npm install
# Build the /dist bundle
npm run prod
# Start the server
npm start
```

### Description

I created this project some years ago for the purpouse to have some kind of demo to benchmark websocket performance on nodejs.

The idea With WS canvas is that all that the user is drawing on the board is not acctually drawn direcly but is sent to the serverside first where it's the broadcasted to all clients that are subscribed, this way what's on the screen should always be equal on each clients screen because everything is drawn syncronously. The server also keeps a buffer off everything that has been drawn so far so that clients can resume even if the connection is broken.

The first version used json as transport between the server and client and this format was offcourse more readable, at some point i wanted to try out how to squeeze every last bit of data transfer and changed the data to binary, i don't think that there was much observable gain in performance even though the data length was sqeezed to just a fraction and i got rid off the json parse overhead, i think my motivation just was to learn how to do this so that i could use this in a web based game later.

My latest update to this project was to rewrite it from one messy app.js by split it to es6 modules by using webpack for bundling, offcourse modern browsers could load es6 modules without a bundler so this doesn't make much sense if the code isn't compiled for backwards compability. I also added eslint with airbnb presets + prettier to clean up and format the code to be more readable and forcing me to use pre opinioned conventions. Theese tools helped me to clean up dead code and bad coding practises.

### TODOs

- More features
- Maybe split events.js actions to a separate actions.js
- Either ditch webpack and go with es6 modules or add something like babel to compile it for older browsers.
- Write a bether serverside app with error handling and debug
