const port = 3000
const wsport = 3100
const express = require('express')

const app = express()
app.use('/', express.static(`${__dirname}/dist`)) // redirect root

app.listen(port)

let buffer = []
const WebSocket = require('ws')

const wss = new WebSocket.Server({ port: wsport })

// Broadcast to all.
wss.broadcast = function broadcast(data) {
  wss.clients.forEach(client => {
    if (client.readyState === WebSocket.OPEN) {
      client.send(data)
    }
  })
}

wss.on('connection', ws => {
  // eslint-disable-next-line no-console
  console.log('ws connection')
  ws.on('message', message => {
    const responseMessage = message
    if (message[0] === 1) {
      // eslint-disable-next-line no-console
      console.log('initialized: clients', wss.clients)
      for (let i = 0, l = buffer.length; i < l; i += 1) {
        ws.send(buffer[i])
      }
      return
    }
    buffer.push(responseMessage)
    if (message[0] === 2) {
      buffer = []
      responseMessage[0] = 4
    }
    wss.clients.forEach(client => {
      client.send(responseMessage)
    })
  })
})
