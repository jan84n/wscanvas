import { getRandomColor, hexToRgb, rgbToHex } from './utils';

let color = getRandomColor();
let canvas = null;
let ctx = null;
let effect = 0;

function initCanvas(canvasEl) {
  canvas = canvasEl;
  ctx = canvas.getContext('2d');
}

function setEffect(eff) {
  effect = eff;
}
function getEffect() {
  return effect;
}

function dotSend(x, y, w, h) {
  color = getRandomColor();
  // return; // TODO: Tää näytti mageemalt
  const rgb = hexToRgb(color);
  const xx = Math.round((x / w) * canvas.width);
  const yy = Math.round((y / h) * canvas.height);
  const size = Math.round(canvas.width / 1000) * 5;
  const bytearray = new Uint16Array([16, xx, yy, size, rgb.r, rgb.g, rgb.b]);
  return bytearray;
  // app.ws.send(bytearray);

  // app.ws.send(JSON.stringify({ dot: [xx, yy, size, color] }));

  // console.log(xx, yy);
  // ctx.beginPath();
  // ctx.arc(xx, yy, size, 0, 2 * Math.PI);
  // ctx.fillStyle = 'black';
  // ctx.fill();
  // ctx.lineWidth = 5;
  // ctx.strokeStyle = '#003300';
  // ctx.stroke();
}

function dotDraw(xx, yy, size, r, g, b) {
  ctx.beginPath();
  ctx.arc(xx, yy, size, 0, 2 * Math.PI);
  ctx.fillStyle = rgbToHex(r, g, b);
  ctx.fill();
}

function lineSend(x1, y1, x2, y2, w, h) {
  // const color = this.color;
  const rgb = hexToRgb(color);
  const xx1 = Math.round((x1 / w) * canvas.width);
  const yy1 = Math.round((y1 / h) * canvas.height);
  const xx2 = Math.round((x2 / w) * canvas.width);
  const yy2 = Math.round((y2 / h) * canvas.height);
  const sz = effect === 1 ? 1 : 10;
  const size = Math.round(canvas.width / 1000) * sz;

  const bytearray = new Uint16Array([
    8,
    xx1,
    yy1,
    xx2,
    yy2,
    size,
    rgb.r,
    rgb.g,
    rgb.b,
  ]);
  return bytearray;
}
function lineDraw(x1, y1, x2, y2, size, r, g, b) {
  ctx.beginPath();
  ctx.moveTo(x1, y1);
  ctx.lineTo(x2, y2);
  ctx.lineWidth = size;
  ctx.strokeStyle = rgbToHex(r, g, b);
  ctx.stroke();
}
// clears the whole canvas
function reset() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function draw(data) {
  const ta = new Uint16Array(data);
  if (ta[0] === 4) {
    reset();
    return;
  }
  if (ta[0] === 16) {
    dotDraw(ta[1], ta[2], ta[3], ta[4], ta[5], ta[6], ta[7], ta[8]);
  }
  if (ta[0] === 8) {
    lineDraw(ta[1], ta[2], ta[3], ta[4], ta[5], ta[6], ta[7], ta[8]);
  }
}

export {
  dotSend,
  dotDraw,
  lineSend,
  lineDraw,
  reset,
  draw,
  initCanvas,
  setEffect,
  getEffect,
};
