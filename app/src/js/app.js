import initEvents from './events';
import '../scss/style.scss';

function init() {
  console.log('init');
  initEvents(document.getElementById('myCanvas'));
}

window.addEventListener('load', init, false);

/*
 * Set the width of the side navigation to 250px and the left margin of the page
 * content to 250px and add a black background color to body
 */
function openNav() {
  document.getElementById('mySidenav').setAttribute('style', 'left: 0');
  //    document.getElementById("main").style.marginLeft = "250px";
  document.getElementById('main').classList.add('blur');
  document.body.style.backgroundColor = 'rgba(0,0,0,0.4)';
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
function closeNav() {
  document.getElementById('mySidenav').setAttribute('style', 'left: -250px');
  //    document.getElementById("main").style.marginLeft = "0";
  document.getElementById('main').classList.remove('blur');
  document.body.removeAttribute('style');
}

function toggleMenu() {
  if (document.getElementById('main').classList.contains('blur')) {
    closeNav();
  } else {
    openNav();
  }
}

window.toggleMenu = toggleMenu;
