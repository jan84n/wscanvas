import { draw } from './canvas';

const wsURL = `ws://${window.location.hostname}:3100`; // (wsURL === null ? window.location.host : wsURL );
const ws = new WebSocket(wsURL);

export default function wsSend(data) {
  ws.send(data);
}

function open() {
  console.log('Connection open');
  const bytearray = new Uint16Array([1]);
  ws.send(bytearray.buffer);
}

function message(evt) {
  draw(evt.data);
}

function close() {
  console.log('Connection is closed...');
  document.querySelector('#main').innerHTML =
    'Connection closed. Please try to reconnect';
}
if ('WebSocket' in window) {
  console.log('WebSocket is supported by your Browser!');
  ws.binaryType = 'arraybuffer';
  ws.onopen = open;
  ws.onmessage = message;
  ws.onclose = close;
} else {
  // The browser doesn't support WebSocket
  console.log('WebSocket NOT supported by your Browser!');
}
