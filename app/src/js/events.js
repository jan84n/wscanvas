import wsSend from './ws';
import { dotSend, lineSend, initCanvas, setEffect, getEffect } from './canvas';

let canvas = null;
let x = null;
let y = null;
let press = false;
let mtime = 0;

function resetCanvas() {
  const bytearray = new Uint16Array([2]);
  wsSend(bytearray.buffer);
}

function selectline() {
  setEffect(0);
}

function selectEffect() {
  setEffect(1);
}

function handleStart(e, clientX, clientY) {
  press = true;
  // console.log('mStart',e);
  const b = canvas.getBoundingClientRect();
  x = clientX - b.left + 1;
  y = clientY - b.top + 1;
  const bytearray = dotSend(x, y, b.width, b.height);
  wsSend(bytearray);
  e.preventDefault();
}

function handleMove(e, clientX, clientY) {
  const tick = Date.now();
  if (tick - mtime < 20) {
    // console.log(tick);
    return;
  }
  mtime = tick;
  if (press === true && x !== null && y !== null) {
    const b = canvas.getBoundingClientRect();
    const newX = clientX - b.left + 1;
    const newY = clientY - b.top + 1;
    const bytearray = lineSend(x, y, newX, newY, b.width, b.height);
    console.log(bytearray);
    wsSend(bytearray);
    if (getEffect() === 0) {
      x = newX;
      y = newY;
    }
    e.preventDefault();
  }
}

function handleEnd() {
  press = false;
  x = null;
  y = null;
}

export default function initEvents(canvasEl) {
  initCanvas(canvasEl);

  canvas = canvasEl;

  canvas.addEventListener(
    'mousedown',
    e => {
      handleStart(e, e.clientX, e.clientY);
    },
    false,
  );

  canvas.addEventListener(
    'touchstart',
    e => {
      handleStart(e, e.touches[0].clientX, e.touches[0].clientY);
    },
    false,
  );

  canvas.addEventListener(
    'mousemove',
    e => {
      handleMove(e, e.clientX, e.clientY);
    },
    false,
  );

  canvas.addEventListener(
    'touchmove',
    e => {
      handleMove(e, e.touches[0].clientX, e.touches[0].clientY);
    },
    false,
  );

  const events = ['touchcancel', 'touchend', 'mouseup', 'mouseout'];
  events.forEach(event => {
    canvas.addEventListener(
      event,
      e => {
        handleEnd(e);
      },
      false,
    );
  });

  document.querySelector('#btn_resetCanvas').addEventListener(
    'click',
    () => {
      resetCanvas();
    },
    false,
  );
  document.querySelector('#btn_setLineCanvas').addEventListener(
    'click',
    () => {
      selectline();
    },
    false,
  );

  document.querySelector('#btn_setEffectCanvas').addEventListener(
    'click',
    () => {
      selectEffect();
    },
    false,
  );
}
