const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
module.exports = env => {
  console.log('NODE_ENV: ', env) // 'local'
  const devMode =
    typeof env === 'undefined' || typeof env.production === 'undefined'
  return {
    mode: devMode ? 'development' : 'production',
    devtool: 'source-map',
    entry: {
      vendor: './app/src/js/vendor.js',
      app: './app/src/js/app.js'
    },
    output: {
      filename: devMode ? '[name].bundle.js' : '[name].bundle.[contenthash].js',
      path: path.resolve(__dirname, 'app/dist')
    },
    plugins: [
      new HtmlWebpackPlugin({
        inject: 'head',
        template: './app/src/index.html'
      }),
      new MiniCssExtractPlugin({
        filename: devMode ? 'app.bundle.css' : 'app.bundle.[contenthash].css'
      })
    ],
    module: {
      rules: [
        {
          test: /\.s[ac]ss$/i,
          use: [
            MiniCssExtractPlugin.loader,
            // Translates CSS into CommonJS
            { loader: 'css-loader', options: { url: false, sourceMap: true } },
            // Compiles Sass to CSS
            { loader: 'sass-loader', options: { sourceMap: true } },
          ]
        }
      ]
    }
  }
}
